#addin Cake.AppCenter
#addin SSP.Build.CakeCore

var target = Argument("target", "Default");

// Setup SSP Cake build settings with defaults
var sspBuildSettings = new SSPBuildSettings();
sspBuildSettings.MSBuildToolVersion = MSBuildToolVersion.VS2019;
sspBuildSettings.BuildVersionString = "2021.0.0.0";
sspBuildSettings.NuGetVersionString = "21.0.0";
sspBuildSettings.GitCommitHash = "abcdef";

Task("Initialize")
  .Does(() =>
{
});

Task("Update Version Numbers")
  .Does(() =>
{
	var buildVersion = EnvironmentVariable("SSP_BUILD_VERSION") ?? "21.0.0";
  if(string.IsNullOrEmpty(buildVersion)) 
	{
    throw new InvalidOperationException("Could not resolve SSP_BUILD_VERSION.");
  }

	var mimsVersion = EnvironmentVariable("SSP_MIMS_VERSION") ?? "21.0.0.0";
  if(string.IsNullOrEmpty(mimsVersion)) 
	{
    throw new InvalidOperationException("Could not resolve SSP_MIMS_VERSION.");
  }

	var buildNumber = EnvironmentVariable("APPCENTER_BUILD_ID") ?? "";
  if(string.IsNullOrEmpty(buildNumber)) 
	{
    throw new InvalidOperationException("Could not resolve APPCENTER_BUILD_ID.");
  }
	
	sspBuildSettings.BuildVersionString = buildVersion + "." + buildNumber;
	sspBuildSettings.NuGetVersionString = sspBuildSettings.BuildVersionString;
	sspBuildSettings.MimsVersion = mimsVersion;

	// Initialize the build settings
	SSPVersionInitialize(sspBuildSettings);
	SSPVersionSet(sspBuildSettings);
});

Task("PreBuild")
  .IsDependentOn("Initialize")
  .IsDependentOn("Update Version Numbers");

Task("PostBuild")
  .IsDependentOn("Initialize")

Task("Default")
  .IsDependentOn("Initialize");

RunTarget(target);