﻿namespace SSP.SI.MIMS.App
{
    using System;
    using Microsoft.AppCenter;
    using Microsoft.AppCenter.Analytics;
    using Microsoft.AppCenter.Crashes;
    using SSP.Mims.Common;
    using SSP.Mims.Common.TinyIoC;
    using SSP.Mims.Mobile.Client;
    using SSP.Mims.Mobile.Client.App;
    using SSP.Mims.Mobile.Client.Search;
    using SSP.Common.Logging;
    using SSP.Mims.Common.Edit;
    using Xamarin.Essentials;
    using Xamarin.Forms.Xaml;
    using SSP.Mims.Mobile.Client.Edit;

    /// <summary>
    /// Class SIApp.
    /// Implements the <see cref="SSP.Mims.Mobile.Client.App.App" />
    /// </summary>
    /// <seealso cref="SSP.Mims.Mobile.Client.App.App" />
    public class SIApp : MimsMobileApp
    {
        /// <summary>
        /// The log
        /// </summary>
        private ILogger log = LogManager.GetLogger(typeof(SIApp));

        /// <summary>
        /// Registers the types.
        /// </summary>
        /// <param name="container">The container.</param>
        protected override void RegisterTypes(IContainerProvider container)
        {
            IDeviceInfo devInfo = container.Resolve<IDeviceInfo>();
            System.Diagnostics.Debug.WriteLine(FileSystem.AppDataDirectory);
            try
            {
                this.log.Enter(nameof(this.RegisterTypes));

                // TODO: Update with project customizations
                //container.RegisterMultipleSingleton<IMimsSearchPlugin>(new Type[]
                //{
                //    typeof(FirstLayerSearchView),
                //});

                //container.RegisterMultipleSingleton<BaseEditFeatureManager>(new Type[]
                //{
                //    typeof(GasPatrolInspectionFeatureManager)
                //});
            }
            finally
            {
                this.log.Exit(nameof(this.RegisterTypes));
            }
        }

        /// <summary>
        /// Registers the views.
        /// </summary>
        /// <param name="registry">The registry.</param>
        protected override void RegisterViews(IViewViewModelRegistrationService registry)
        {
            try
            {
                this.log.Enter(nameof(this.RegisterViews));

                // TODO: Update with project customizations
                // Search
                //registry.Register<FirstLayerSearchViewModel, FirstLayerSearchView>();
                //registry.Register<FirstLayerResultsViewModel, FirstLayerResultsView>();

                // Inspections
                //registry.Register<GasValveInspectionOverviewViewModel, GasValveInspectionOverviewView>(nameof(GasValveInspectionOverviewView));
                //registry.Register<GasValveInspectionFeatureViewModel, GasValveInspectionFeatureView>(nameof(GasValveInspectionFeatureView));

                // Custom panel
                //registry.Register<GpsPatrolViewModel, GpsPatrolView>();
            }
            finally
            {
                this.log.Exit(nameof(this.RegisterViews));
            }
        }

        /// <summary>
        /// Initializes Microsoft AppCenter.
        /// </summary>
        protected override void InitializeAppCenter()
        {
            try
            {
                this.log.Enter(nameof(this.InitializeAppCenter));

                AppCenter.Start("ios=b66ae42e-059e-42f2-a17b-497e12ae5adb;",
                    typeof(Analytics), typeof(Crashes));
            }
            finally
            {
                this.log.Exit(nameof(this.InitializeAppCenter));
            }
        }
    }
}